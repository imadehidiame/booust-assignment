var Animal_Kingdom = /** @class */ (function () {
    //constructor
    function Animal_Kingdom(animal_name) {
        this.count = 0;
        this.construct_count = 0;
        this.correct_count = 0;
        this.animal_name = animal_name;
        this.construct_count++;
    }
    Animal_Kingdom.prototype.return_cons_count = function () {
        return this.construct_count;
    };
    Animal_Kingdom.prototype.show_animals = function (div) {
        //for(var key in this.animals_list){
        //var item: string = this.animals_list[key];
        var img = document.createElement("img");
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.className = "select_animal";
        checkbox.value = this.animal_name;
        //checkbox.checked=true;
        img.src = "animals/" + this.animal_name + ".jpg";
        img.width = 220;
        img.height = 220;
        var sel = this;
        img.onclick = function () {
            //var get_item = item;
            sel.alert_animal_name(alert);
        };
        var inner_div = document.createElement("div");
        if (this.return_cons_count() <= 4) {
            inner_div.className = "one";
        }
        else if (this.return_cons_count() > 4 && this.return_cons_count()) {
            inner_div.className = "two";
        }
        else {
            inner_div.className = "three";
        }
        inner_div.innerText = this.animal_name;
        inner_div.appendChild(img);
        inner_div.appendChild(checkbox);
        document.getElementById(div).appendChild(inner_div);
        //}
    };
    Animal_Kingdom.prototype.show_ticked = function () { };
    Animal_Kingdom.prototype.increase_ticked_count = function () {
        this.count++;
    };
    Animal_Kingdom.prototype.decrease_ticked_count = function () {
        this.count--;
    };
    Animal_Kingdom.prototype.return_correct_count = function () {
        return this.correct_count;
    };
    Animal_Kingdom.prototype.return_ticked_count = function () {
        return this.count;
    };
    Animal_Kingdom.prototype.show_animals_result = function (div, animal) {
        //for(var key in this.animals_list){
        //var item: string = this.animals_list[key];
        var arr = this.create_new_array();
        var img = document.createElement("img");
        //for(var item in arr){
        img.src = "animals/" + animal + ".jpg";
        img.width = 80;
        img.height = 80;
        var inner_div = document.createElement("div");
        inner_div.appendChild(img);
        document.getElementById(div).appendChild(inner_div);
        //}
        //}
    };
    Animal_Kingdom.prototype.show_button = function (div) {
        var _this = this;
        var checkbox = document.createElement("input");
        checkbox.type = "submit";
        checkbox.value = "Submit";
        checkbox.className = "btn";
        var checkbox1 = document.createElement("input");
        checkbox1.type = "text";
        checkbox1.id = "selected_box";
        checkbox1.readOnly = true;
        checkbox1.className = "text";
        var sel = this;
        var inner_div = document.createElement("div");
        var inner_div1 = document.createElement("div");
        inner_div.id = "lastDiv";
        inner_div1.id = "lastDiv1";
        inner_div1.innerHTML = "<h3 id=\"styleh2\">RULES OF THE ANIMAL KINGDOM GAME</h3>There are 12 Animals on display, \n        arranged in three rows and four columns.Animal Kingdom randomly selects 3 Animals from the List in no \n        particular order and expects you to \"Make Exactly 3 Selections from the List (<b>NO MORE, NO LESS</b>). \n        You will not be able to make more than 3 selections even if you tried!!\". The aim of this game is to find out \n        the strength of your <b>GUESS POWER</b>. Results are displayed after exactly 3 Selections have been made by you and the \n         \"Submit\" button is clicked.<br>N.B Please take every advice you read from the Verdict..Best of luck PLAYER";
        var message_container = document.createElement("div");
        message_container.id = "MsgContainer";
        var SuccMsg = document.createElement("div");
        SuccMsg.id = "SuccMsg";
        var ErrMsg = document.createElement("div");
        ErrMsg.id = "ErrMsg";
        inner_div.innerText = "Selected Count";
        inner_div.appendChild(checkbox1);
        inner_div.appendChild(checkbox);
        inner_div.appendChild(inner_div1);
        //message_container.appendChild(SuccMsg);
        //message_container.appendChild(ErrMsg);
        document.getElementById(div).appendChild(message_container);
        document.getElementById(div).appendChild(inner_div);
        var self = this;
        checkbox.onclick = function () {
            _this.clear_div("MsgContainer");
            var real_arr = sel.create_new_array();
            var checked_arr = sel.create_checked_array();
            if (checked_arr.length !== 3) {
                _this.verdict_error("MsgContainer", "Rule says, \"You must make exactly 3 selections\". You've made " + checked_arr.length + " selection(s)");
                return false;
            }
            for (var item in checked_arr) {
                if (_this.in_array(checked_arr[item], real_arr)) {
                    _this.correct_count++;
                }
            }
            var verdict1 = "CORRECT CHOICE WOULD HAVE BEEN<ul>";
            for (var itemKey in real_arr) {
                verdict1 += "<li>" + real_arr[itemKey] + "</li>";
            }
            verdict1 += "</ul>";
            var verdict = "";
            if (_this.return_correct_count() === 3) {
                verdict = "Score: " + _this.return_correct_count() + "/" + real_arr.length + " Verdict: Wow!! You'll earn alot from Magic";
            }
            else if (_this.return_correct_count() === 2) {
                verdict = "Score: " + _this.return_correct_count() + "/" + real_arr.length + " Verdict: Almost there. You'll get there surely";
            }
            else if (_this.return_correct_count() === 1) {
                verdict = "Score: " + _this.return_correct_count() + "/" + real_arr.length + " Verdict: A bit too far from the target";
            }
            else {
                verdict = "Score: " + _this.return_correct_count() + "/" + real_arr.length + " Verdict:Bit of advice, \"Don't ever play a Lotto Game!!\"";
            }
            _this.verdict_div("MsgContainer", verdict, verdict1);
            _this.correct_count = 0;
        };
    };
    Animal_Kingdom.prototype.verdict_div = function (outer_div, verdict, correct) {
        var SuccMsg = document.createElement("div");
        SuccMsg.id = "SuccMsg";
        SuccMsg.innerHTML = verdict;
        var SuccMsg1 = document.createElement("div");
        SuccMsg1.id = "SuccMsg1";
        SuccMsg1.innerHTML = correct;
        document.getElementById(outer_div).appendChild(SuccMsg1);
        document.getElementById(outer_div).appendChild(SuccMsg);
    };
    Animal_Kingdom.prototype.verdict_error = function (outer_div, verdict) {
        var ErrMsg = document.createElement("div");
        ErrMsg.id = "ErrMsg";
        ErrMsg.innerHTML = verdict;
        document.getElementById(outer_div).appendChild(ErrMsg);
    };
    Animal_Kingdom.prototype.clear_div = function (outer_div) {
        document.getElementById(outer_div).innerHTML = "";
    };
    Animal_Kingdom.prototype.in_array = function (key, array) {
        var count = false;
        for (var item in array) {
            if (key === array[item]) {
                count = true;
                break;
            }
        }
        return count;
    };
    Animal_Kingdom.prototype.create_new_array = function () {
        var animals = document.getElementsByClassName("select_animal");
        var all_arr = [];
        var select_arr = [];
        for (var item in animals) {
            var htmlelement = animals[item];
            all_arr.push(htmlelement.value);
        }
        for (var i = 0; i < 3; i++) {
            var animal = all_arr[this.get_random_int(0, 12)];
            if (this.in_array(animal, select_arr)) {
                i -= 1;
            }
            else {
                select_arr.push(animal);
            }
        }
        return select_arr;
    };
    Animal_Kingdom.prototype.create_checked_array = function () {
        var animals = document.getElementsByClassName("select_animal");
        var all_arr = [];
        var select_arr = [];
        for (var item in animals) {
            var htmlelement = animals[item];
            if (htmlelement.checked || htmlelement.checked === true)
                all_arr.push(htmlelement.value);
        }
        return all_arr;
    };
    Animal_Kingdom.prototype.return_animal_name = function () {
        return this.animal_name;
    };
    Animal_Kingdom.prototype.get_random_int = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    Animal_Kingdom.prototype.get_all_animals = function (callback) {
        var animals = document.getElementsByClassName("select_animal");
        for (var item in animals) {
            var htmlelement = animals[item];
            callback(htmlelement.value);
        }
    };
    Animal_Kingdom.prototype.alert_animal_name = function (callback) {
        //this.animal_name=i;
        callback("Animal's name is " + this.animal_name);
    };
    Animal_Kingdom.prototype.get_count_function = function (div) {
        var d = document.getElementById(div);
        d.value = "" + this.return_ticked_count();
    };
    return Animal_Kingdom;
}());
var animals_list = ["antelope", "ape", "cat", "cheetah", "crocodile", "doe", "elephant", "leopard",
    "lion", "monkey", "rhino", "tiger"];
//let animals : Animal_Kingdom[];
for (var key in animals_list) {
    var item = animals_list[key];
    var animal = new Animal_Kingdom(item);
    animal.show_animals("container1");
}
var animal = new Animal_Kingdom();
animal.show_button("container1");
var sels = document.getElementsByClassName("select_animal");
for (var i in sels) {
    sels[i].onclick = function () {
        if ((!this.checked || this.checked === false) && animal.return_ticked_count() > 0) {
            animal.decrease_ticked_count();
        }
        if ((this.checked || this.checked === true) && animal.return_ticked_count() >= 0) {
            animal.increase_ticked_count();
            if (animal.return_ticked_count() > 3) {
                this.checked = false;
                animal.decrease_ticked_count();
            }
        }
        animal.get_count_function("selected_box");
    };
}
//animal.get_all_animals(console.log);
